[![pipeline status](https://gitlab.com/TheAlkaris/protonmail-theme-dark/badges/master/pipeline.svg)](https://gitlab.com/TheAlkaris/protonmail-theme-dark/commits/master)
[![build status](https://img.shields.io/badge/build-passing-brightgreen.svg)](https://gitlab.com/TheAlkaris/protonmail-theme-dark/commits/master)

# ProtonMail Arc Darker theme
My custom theme for ProtonMail

Theme based on [Arc Darker](https://github.com/LinxGem33/OSX-Arc-Darker "OSX Arc Dark") the Linux desktop theme.

![protonmail osx arc dark theme](/shutter__2018-01-15__11_00_30_PM.png)


You can load the CSS by doing the following

```CSS
@import url("https://gitlab.com/TheAlkaris/protonmail-theme-dark/-/blob/e9aa94eef062b8342670f6b85157f3b4f15dd6ff/osx-arc-dark.css");
```

if you want to modify the colors just edit the first line segment with the following;

```CSS
:root {
	--theme1:#636B80;
	--theme2:#AFB8C6;
	--theme3:#383C4A;
	--theme4:#383C4A;
	--theme5:#AFB8C6;
	--theme6:#4F5666;
	--theme7:#404552;
	--red: #F25056;
	--red-light: #C75738;
	--yellow: #FAC536;
	--yellow-light: #E6B15C;
	--green: #39EA49;
	--green-light: #A6CC7A;
	--blue: #8777D9;
	--blue-light: #B2A3FF;
	--white: #AFB8C6;
	--black: #000
}
```

Don't ask me to maintain this or help you with issues you may be having while using this theme, I'm still not very good with CSS. If you have any suggestions for improving this, then submit them to issues.
